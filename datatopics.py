from elasticsearch import Elasticsearch
from gensim import corpora, models
from utils import Utils

# Retrieves all the publications
def get_publications():
    result = es.search(index="publication_user", doc_type="publication")
    size = result["hits"]["total"]
    query_match_all = {
        "size": size,
        "query": {
            "match_all": {}
        }
    }
    return es.search(index="publication_user", doc_type="publication", body=query_match_all)["hits"]["hits"]

# Retrieves the general topics of a given user
def get_user_general_topics(user_id):
    query_main_topics = {
        "query": {
            "term": {
                "userId": user_id
            }
        }
    }
    result = es.search(index="main_topics", doc_type="user_topics", body=query_main_topics)
    result = result["hits"]["hits"][0]["_source"]["maintopics"]
    return result

# This method runs the lda algorithm for topics extraction
def extract_topics(text, size):
    # Generate dictionary
    dict = corpora.Dictionary(text)
    # Generate corpus
    corpus = [dict.doc2bow(text) for text in text]
    # Generate LDA Model
    lda = models.ldamodel.LdaModel(corpus, id2word=dict, num_topics=size)
    return lda


es = Elasticsearch()

utils = Utils(es)

# Delete the "top_topics" index
es.indices.delete(index="top_topics", ignore=[400, 404])

publications = get_publications()
users = utils.get_users()
completeText = ""
tokens = []

for u in users:
    userId = u["_id"]
    topics = get_user_general_topics(userId)

    tempText = ""
    for t in topics:
        tempText += t["topic"] + ' '
    completeText = tempText

    if completeText != "":
        try:
            result_tokens = es.indices.analyze(index="publication_user",
                                               analyzer="lookinlabs_analyzer",
                                               body=completeText)
            partial_tokens = []
            for r in result_tokens["tokens"]:
                partial_tokens.append(r["token"])
            tokens.append(partial_tokens)
        except:
            print(completeText)

lda = extract_topics(tokens, 50)

i = 0
for topic in lda.show_topics(num_topics=50, num_words=20, log=False, formatted=False):
    topic_data = {}
    topic_data["id"] = i
    topic_data["description"] = []
    for t in topic:
        topic_data["description"].append({"key": t[1], "value": t[0]})

    es.index(index="top_topics", doc_type="topic", body=topic_data)
    i += 1
