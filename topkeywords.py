from elasticsearch import Elasticsearch

from utils import Utils


# Returns the uncommon-common keywords of a given user or the top-keywords if there are not uncommon-common
def getKeywordsWithSize(userId, size):
    # query to get the top _size uncommon common keywords of a given user
    query_keywords = {
        "query": {
            "term": {
                "doc.userId": userId
            }
        },
        "aggregations": {
            "most_sig": {
                "significant_terms": {
                    "field": "doc.title_summary",
                    "size": size
                }
            }
        }
    }
    result = es.search(index="publication_user", doc_type="publication", body=query_keywords)
    result = result["aggregations"]["most_sig"]["buckets"]

    if len(result) == 0:
        # query to get the top _size keywords of a given user
        query_keywords = {
            "query": {
                "term": {
                    "doc.userId": userId
                }
            },
            "aggregations": {
                "most_popular": {
                    "terms": {
                        "field": "doc.title_summary",
                        "size": size
                    }
                }
            }
        }
        result = es.search(index="publication_user", doc_type="publication", body=query_keywords)
        result = result["aggregations"]["most_popular"]["buckets"]

    filtered_result = []

    try:
        max = -1
        for item in result:
            if item["score"] > max:
                max = item["score"]
        for item in result:
            size = item["score"] * 40 / max
            if size < 20:
                size = 20
            similar = False
            for word in filtered_result:
                if utils.levenshtein_distance(word["key"], item["key"])["distance"] <= 2:
                    similar = True
                    break
            if similar is False:
                if (userId == "user_129713"):
                    print(item["key"])
                filtered_key = \
                    es.indices.analyze(index="publication_user",
                                       analyzer="lookinlabs_analyzer",
                                       body=item["key"])["tokens"][0]["token"]
                filtered_result.append({"key": filtered_key, "size": size})
                if len(filtered_result) == 40:
                    break
    except:
        print(userId)

    return filtered_result


es = Elasticsearch()

utils = Utils(es)

# Deletes the "top_keywords" index
es.indices.delete(index="top_keywords", ignore=[400, 404])

mapping = {
    "mappings": {
        "keyword": {
            "properties": {
                "keywords": {
                    "properties": {
                        "name": {
                            "type": "string"
                        },
                        "size": {
                            "type": "string"
                        }
                    }
                },
                "userId": {
                    "type": "string"
                }
            }
        }
    }
}

# Creates the "top_keywords" index with a specified mapping
es.indices.create(index="top_keywords", body=mapping)

users = utils.get_users()

for u in users:
    userId = u["_id"].replace("user_", "")
    result = es.get(index="nested_users", doc_type="nestedUser", id=userId)

    keywords = {}
    keywords["keywords"] = getKeywordsWithSize("user_" + userId, 200);
    keywords["userId"] = "user_" + userId

    es.index(index="top_keywords", doc_type="keyword", body=keywords)
