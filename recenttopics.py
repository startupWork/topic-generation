import nltk

from datetime import date
from elasticsearch import Elasticsearch
from nltk.collocations import *
from utils import Utils


# Returns the uncommon-common keywords of a given user or the top-keywords if there are not uncommon-common
def get_keywords(user_id, size):
    # Query to get the top "size" uncommon common keywords of a given user
    query_keywords = {
        "query": {
            "term": {
                "doc.userId": user_id
            }
        },
        "aggregations": {
            "most_popular": {
                "terms": {
                    "field": "doc.title_summary",
                    "size": size
                }
            }
        }
    }
    result = es.search(index="publication_user", doc_type="publication", body=query_keywords)
    result = result["aggregations"]["most_popular"]["buckets"]

    try:
        for i in range(0, len(result)):
            result[i]["key"] = \
                es.indices.analyze(index="publication_user",
                                   analyzer="lookinlabs_analyzer",
                                   body=result[i]["key"])["tokens"][0]["token"]
    except:
        print("No results")

    return result

# Retrieves all the publications of a given user
def get_publications_by_user(user_id):
    query_publications_by_user = {
        "size": 20,
        "query": {
            "term": {
                "doc.userId": user_id
            }
        },
        "sort": [
            {"doc.year": {"order": "desc", "mode": "avg"}}
        ]
    }
    result = es.search(index="publication_user", doc_type="publication", body=query_publications_by_user)
    result = result["hits"]["hits"]
    return result

# Gets the bigrams of a given "freq" in a given "text"
def get_bigrams(text, freq):
    bigram_measures = nltk.collocations.BigramAssocMeasures()
    result_tokens = es.indices.analyze(index="publication_user", analyzer="lookinlabs_analyzer", body=text)
    tokens = []
    for r in result_tokens["tokens"]:
        tokens.append(r["token"])
    finder = BigramCollocationFinder.from_words(tokens)
    if freq is not None:
        finder.apply_freq_filter(freq)
    bigrams = finder.nbest(bigram_measures.likelihood_ratio, 200)
    return bigrams

# Gets the trigrams of a given "freq" in a given "text"
def get_trigrams(text, freq):
    trigram_measures = nltk.collocations.TrigramAssocMeasures()
    result_tokens = es.indices.analyze(index="publication_user", analyzer="lookinlabs_analyzer", body=text)
    tokens = []
    for r in result_tokens["tokens"]:
        tokens.append(r["token"])
    finder = TrigramCollocationFinder.from_words(tokens)
    if freq is not None:
        finder.apply_freq_filter(freq)
    trigrams = finder.nbest(trigram_measures.likelihood_ratio, 150)
    return trigrams

# Return a list with the different words of a list of trigrams compared to a given bigram
def get_differences(bigram, ltrigrams):
    differences = []
    for t in ltrigrams:
        if t[0] not in bigram:
            differences.append(t[0])
        if t[1] not in bigram:
            differences.append(t[1])
        if t[2] not in bigram:
            differences.append(t[2])
    return differences

# Removes the duplicates of a list of bigrams
def filter_dup_bigrams(lbigrams):
    items = set()
    result = []
    for b in lbigrams:
        temp = b[0] + ' ' + b[1]
        if b not in result and temp not in items:
            items.add(b[0] + ' ' + b[1])
            items.add(b[1] + ' ' + b[0])
            result.append(b)
    return result

# Removes the bigrams of a list of bigrams that contains any keywords of a list of keywords
def filter_bigrams_by_keywords(lbigrams, lkeywords):
    result = []
    for b in lbigrams:
        add = False
        for k in lkeywords:
            if k["key"] in b:
                add = True
                break
        if add:
            result.append(b)
    return result


es = Elasticsearch()

utils = Utils(es)

# Delete the "recent_topics" index
es.indices.delete(index="recent_topics", ignore=[400, 404])

mapping = {
    "mappings": {
        "topic": {
            "properties": {
                "maintopics": {
                    "properties": {
                        "topic": {
                            "type": "string"
                        }
                    }
                },
                "userId": {
                    "type":"string"
                }
            }
        }
    }
}

# Creates the "recent_topics" index with a specified mapping
es.indices.create(index="recent_topics", body=mapping)

users = utils.get_users()

for u in users:
    userId = u["_id"].replace("user_", "")
    result = es.get(index="nested_users", doc_type="nestedUser", id=userId)

    keywords = get_keywords("user_" + userId, 20);

    years = []
    current_year = date.today().year + 1
    since = current_year - 5
    for i in range(since, current_year):
        years.append(i)

    bigrams = []
    trigrams = []
    if len(result["_source"]["doc"]["publicationList"]) > 0:
        completeText = ""
        for publication in result["_source"]["doc"]["publicationList"]:
            if publication["year"] in years:
                title = publication["title"]
                try:
                    summary = publication["summary"]
                except Exception:
                    summary = ""
                completeText += title + ' ' + summary + ' '
        if completeText != "":
            bigrams = get_bigrams(completeText, 3)
            trigrams = get_trigrams(completeText, 2)

    bigrams = filter_dup_bigrams(bigrams)
    bigrams = filter_bigrams_by_keywords(bigrams, keywords)

    map = {}
    for b in bigrams:
        entry = []
        if b in map:
            entry = map[b]
        for t in trigrams:
            if b[0] in t and b[1] in t:
                entry.append(t)
        if len(entry) > 0:
            map[b] = entry

    second_map = {}
    for key in map:
        values = map[key]
        words = get_differences(key, values)
        entry = [[key[0], key[1]]]
        found = False
        for w1 in words:
            for w2 in words:
                if len(w1) != len(w2):
                    d = utils.levenshtein_distance(w1, w2)["distance"]
                    if d <= 3:
                        if len(w1) > len(w2):
                            entry[0].append(w1)
                        else:
                            entry[0].append(w2)
                    found = True
            if found is True:
                break

        if len(entry[0]) > 2:
            second_map[key] = entry
        else:
            second_map[key] = values

    topic_data = {}
    topic_data["userId"] = "user_" + userId
    topic_data["maintopics"] = []

    added_bigrams = []
    added_trigrams = []
    count = 0
    for b in bigrams:
        if count == 5:
            break
        if b in second_map:
            t = second_map[b][0]
            exist_similar = False
            for at in added_trigrams:
                coincidences = 0
                if t[0] in at:
                    coincidences += 1
                if t[1] in at:
                    coincidences += 1
                if t[2] in at:
                    coincidences += 1
                if coincidences >= 2:
                    exist_similar = True
                    break

            if t not in added_trigrams and b not in added_bigrams and not exist_similar:
                try:
                    topic_data["maintopics"].append({"topic": t[0] + ' ' + t[1] + ' ' + t[2]})
                    added_bigrams.append(b)
                    added_trigrams.append(t)
                    count += 1
                except:
                    print(t)

    if count < 5:
        for b in bigrams:
            if count == 5:
                break
            exist_bigram = False
            for ab in added_bigrams:
                if b[0] in ab and b[1] in ab:
                    exist_bigram = True
                    break
            for at in added_trigrams:
                if b[0] in at and b[1] in at:
                    exist_bigram = True
                    break
            if not exist_bigram:
                try:
                    topic_data["maintopics"].append({"topic": b[0] + ' ' + b[1]})
                    added_bigrams.append(b)
                    count += 1
                except:
                    print(b)

    es.index(index="recent_topics", doc_type="topic", body=topic_data)
