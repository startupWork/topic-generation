from elasticsearch import Elasticsearch


class Utils:
    def __init__(self, es):
        self.es = es

    # Retrieves all the users
    def get_users(self):
        result = self.es.search(index="publication_user", doc_type="user")
        size = result["hits"]["total"]
        query_match_all = {
            "size": size,
            "query": {
                "match_all": {}
            }
        }
        return self.es.search(index="publication_user", doc_type="user", body=query_match_all)["hits"]["hits"]

    # Retrieves all the affiliation
    def get_affiliations(self, start):
        # result = self.es.search(index="nested_affiliations", doc_type="nestedAffiliations")
        size = 20
        query_match_all = {
            "size": size,
            "query": {
                "match_all": {}
            },
            "search_after": [start],
            "sort" : [
                { "affiliation_id" : {"order" : "asc"}}]
        }
        return self.es.search(index="nested_affiliations", doc_type="nestedAffiliations", body=query_match_all)["hits"]["hits"]

    # Returns the uncommon-common keywords of a given user or the top-keywords if there are not uncommon-common
    def get_keywords(self, userId, size):
        # Query to get the top "size" uncommon common keywords of a given user
        query_keywords = {
            "query": {
                "term": {
                    "doc.userId": userId
                }
            },
            "aggregations": {
                "most_sig": {
                    "significant_terms": {
                        "field": "doc.title",
                        "size": size
                    }
                }
            }
        }
        result = self.es.search(index="publication_user", doc_type="publication", body=query_keywords)
        result = result["aggregations"]["most_sig"]["buckets"]
        if len(result) == 0:
            # Query to get the top "size" keywords of a given user
            query_keywords = {
                "query": {
                    "term": {
                        "doc.userId": userId
                    }
                },
                "aggregations": {
                    "most_popular": {
                        "terms": {
                            "field": "doc.title",
                            "size": size
                        }
                    }
                }
            }
            result = self.es.search(index="publication_user", doc_type="publication", body=query_keywords)
            result = result["aggregations"]["most_popular"]["buckets"]
        return result

    # Returns the levenshtein distance between to strings
    def levenshtein_distance(self, str1, str2):
        m = len(str1)
        n = len(str2)
        lensum = float(m + n)
        d = []
        for i in range(m + 1):
            d.append([i])
        del d[0][0]
        for j in range(n + 1):
            d[0].append(j)
        for j in range(1, n + 1):
            for i in range(1, m + 1):
                if str1[i - 1] == str2[j - 1]:
                    d[i].insert(j, d[i - 1][j - 1])
                else:
                    minimum = min(d[i - 1][j] + 1, d[i][j - 1] + 1, d[i - 1][j - 1] + 2)
                    d[i].insert(j, minimum)
        ldist = d[-1][-1]
        ratio = (lensum - ldist) / lensum
        return {"distance": ldist, "ratio": ratio}
